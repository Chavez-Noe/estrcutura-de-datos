#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int value;
    struct Node *ptrNext; // Se define strcut, ya que marca warrning, debido a que como se usa Node, pero el alias an no se define hasta la linea siguente, no sabe qué tipo es el compilador.
} Node;

typedef struct List
{
    int size;
    Node *ptrhead; // Aquí ya no es necesario el strcut ya que el compilador ya sabe de qué tipo es ese alieas.
} List;

List *create_list();
void insert_principio(List *list, int value);
void insert_final(List *list, int value);
void print(List *list);

int main(int argc, char const *argv[])
{

    List *list = create_list();
    insert_principio(list, 5);
    insert_principio(list, 8);
    insert_principio(list, 30);
    insert_principio(list, 45);

    print(list);

    insert_final(list, 9);
    insert_final(list, 8);
    insert_final(list, 89);

    print(list);

    return 0;
}

List *create_list()
{
    List *list = (List *)malloc(sizeof(List));
    list->size = 0;
    list->ptrhead = NULL;
    return list;
}

void insert_principio(List *list, int value)
{
    Node *new_node = (Node *)malloc(sizeof(Node));
    // Asignamos el valor
    new_node->value = value;
    // Verficar si la lista está vacia o tiene elementos.
    if (list->size == 0)
    {
        list->ptrhead = new_node;
        new_node->ptrNext = NULL;
    }
    else
    { // En caso de haber más de un nodo.
        Node *old_head = list->ptrhead;
        list->ptrhead = new_node; // reemplazamos la cabeza vieja por la nueva.
        new_node->ptrNext = old_head;
    }
    list->size += 1;
}

void insert_final(List *list, int value)
{
    Node *new_node = (Node *)malloc(sizeof(Node));
    new_node->value = value;

    if (list->size == 0)
    {
        list->ptrhead = new_node;
        new_node->ptrNext = NULL;
    }
    else
    {
        while (list->ptrhead != NULL)
        {
            list = list->ptrhead;
        }

        list->ptrhead = new_node;
        new_node->ptrNext = NULL;
    }
    list->size += 1;
}

void print(List *list) {
    while (list->ptrhead != NULL)
    {
        printf("%i ", list->ptrhead->value);
        list = list->ptrhead;
    }
    printf("\n");
}